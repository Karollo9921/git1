def solution(a, b):
    if b == 0:
        return 1
    elif b == 1:
        return a
    else:
        for i in range(1, b+1):
            if i == 1:
                n = a
            else:
                a = n*a
        return a


print(solution(2, 7))